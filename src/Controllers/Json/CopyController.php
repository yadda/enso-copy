<?php

namespace Yadda\Enso\Copy\Controllers\Json;

use Exception;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Yadda\Enso\Copy\Facades\EnsoCopy;

class CopyController
{
    use ValidatesRequests;

    public function index(Request $request): JsonResponse
    {
        $items = (new Collection($request->input('items', [])))->map(function ($type, $slug) {
            return (in_array($type, EnsoCopy::types()))
                ? ($value = EnsoCopy::$type($slug))
                : $value = null;
        });

        return Response::json($items, 200);
    }

    public function show(Request $request, string $copy, string $type): JsonResponse
    {
        if (in_array($type, EnsoCopy::types())) {
            $value = EnsoCopy::$type($copy);
        } else {
            $value = null;
        }

        if (is_null($value)) {
            abort(404);
        }

        return Response::json($value, 200);
    }
}
