<?php

namespace Yadda\Enso\Copy\Controllers\Admin;

use Yadda\Enso\Copy\Contracts\CopyControllerContract;
use Yadda\Enso\Crud\Controller;

class CopyController extends Controller implements CopyControllerContract
{
    protected $crud_name = 'copy';
}
