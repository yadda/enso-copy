<?php

namespace Yadda\Enso\Copy\Crud;

use Illuminate\Validation\Rule;
use Yadda\Enso\Copy\Contracts\CopyContract;
use Yadda\Enso\Copy\Contracts\CopyCrudContract;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Utilities\Helpers;

class Copy extends Config implements CopyCrudContract
{
    /**
     * Defines configuration for this CRUD item
     *
     * @return void
     */
    public function configure()
    {
        $copy_model = Helpers::getConcreteClass(CopyContract::class);

        $this->model($copy_model)
            ->route('admin.copy')
            ->views('copy')
            ->name('Copy')
            ->setNamePlural('Copy')
            ->paginate(25)
            ->columns([
                Text::make('name'),
            ])
            ->order('name', 'ASC')
            ->filters([
                'name' => [
                    'type' => 'text',
                    'label' => 'Search',
                    'default' => null,
                    'props' => [
                        'placeholder' => 'Search by Copy name',
                    ],
                ],            ])
            ->rules([
                'main.name' => ['required', Rule::unique($copy_model, 'name')],
            ]);
    }

    /**
     * Default form configuration.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('name'),
                    SlugField::make('slug'),
                    WysiwygField::make('content')
                        ->setHelpText(
                            'In some places where this text is used formatting '
                            . 'applied here may be ignored or overridden.'
                        ),
                ]),
        ]);

        return $form;
    }
}
