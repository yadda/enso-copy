<?php

namespace Yadda\Enso\Copy\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for Enso Copy
 */
class EnsoCopy extends Facade
{
    /**
     * Get the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'ensocopy';
    }
}
