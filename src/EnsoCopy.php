<?php

namespace Yadda\Enso\Copy;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Yadda\Enso\Copy\Contracts\CopyContract;
use Yadda\Enso\Utilities\Helpers;

class EnsoCopy
{
    /**
     * Array of valid types of content
     *
     * @return array
     */
    public static function types(): array
    {
        return [
            'text',
            'html',
        ];
    }

    /**
     * Clears the cached data for the given copy item
     *
     * @param string $slug
     *
     * @return void
     */
    public static function clear(string $slug): void
    {
        Cache::forget(static::key($slug, 'text'));
        Cache::forget(static::key($slug, 'html'));
    }

    /**
     * The html version of a given piece of copy. If this content is empty, or
     * if this copy item does not exist and empty string is returned.
     *
     * @param string $slug
     * @param string $content
     *
     * @return string|null
     */
    public function html(string $slug, string $content = null): ?string
    {
        $value = Cache::rememberForever(
            static::key($slug, 'html'),
            function () use ($slug) {
                return static::fetch($slug, 'content');
            }
        );

        if (!$value && $content) {
            return $content;
        }

        return $value;
    }

    /**
     * Add frontend routes for copy
     */
    public function routes(string $path, string $class, string $name): void
    {
        Route::prefix($path)->name($name . '.')->group(function () use ($class) {
            Route::get('/')
                ->uses([$class, 'index'])
                ->name('index');
            Route::get('{copy}/{type}')
                ->uses([$class, 'show'])
                ->name('show');
        });
    }

    /**
     * The text-only version of a given piece of copy. If this content is empty,
     * or if this copy item does not exist and empty string is returned.
     *
     * @param string $slug
     *
     * @return string|null
     */
    public function text(string $slug, string $content = null)
    {
        $value = Cache::rememberForever(
            static::key($slug, 'text'),
            function () use ($slug) {
                return static::fetch($slug, 'parsed');
            }
        );

        if (!$value && $content) {
            return $content;
        }

        return $value;
    }

    /**
     * Fetches a single Copy record from the database and stashes the result
     * locally
     *
     * @param string $slug
     * @param string $property
     *
     * @return string|null
     */
    protected static function fetch(string $slug, string $property): ?string
    {
        $copy = Helpers::getConcreteClass(CopyContract::class)::where('slug', $slug)->first();

        if (!$copy) {
            return null;
        }

        if (!$copy->{$property}) {
            return null;
        }

        return $copy->{$property};
    }

    /**
     * Cache key for a given slug and type of content
     *
     * @param string $slug
     * @param string $type
     *
     * @return string
     */
    protected static function key(string $slug, string $type): string
    {
        return 'copy_' . $slug . '_' . $type;
    }
}
