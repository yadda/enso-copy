<?php

namespace Yadda\Enso\Copy\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Copy\Contracts\CopyContract;
use Yadda\Enso\Copy\Facades\EnsoCopy;
use Yadda\Enso\Crud\Traits\IsCrudModel;

class Copy extends Model implements CopyContract
{
    use HasFactory,
        IsCrudModel;

    /**
     * Name of the database table
     *
     * @var string
     */
    protected $table = 'enso_copy';

    /**
     * The model's attributes
     *
     * @var array
     */
    protected $attributes = [
        'content_json' => '[]',
    ];

    /**
     * Attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'content_json' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'content',
        'content_json',
    ];

    /**
     * The "booting" method of the model.
     */
    public static function boot(): void
    {
        parent::boot();

        static::saving(function ($model) {
            $model->parsed = strip_tags($model->content);
            EnsoCopy::clear($model->slug);
        });
    }

    /**
     * Get the route key for the model.
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
