<?php

namespace Yadda\Enso\Copy;

use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Copy\Contracts\CopyContract;
use Yadda\Enso\Copy\Controllers\Admin\CopyController;
use Yadda\Enso\Copy\Crud\Copy as CopyCrud;
use Yadda\Enso\Copy\EnsoCopy;
use Yadda\Enso\Copy\Models\Copy;

class EnsoCopyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom([
            __DIR__ . '/Database/Migrations/',
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/installable/config/enso/crud/copy.php',
            'enso.crud.copy'
        );

        $this->publishes([
            __DIR__ . '/installable/config' => base_path('config'),
        ], 'enso-config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ensocopy', function () {
            return new EnsoCopy;
        });

        $this->app->bind(CopyContract::class, Copy::class);
        $this->app->bind(CopyCrudContract::class, CopyCrud::class);
        $this->app->bind(CopyControllersContract::class, CopyController::class);
    }
}
