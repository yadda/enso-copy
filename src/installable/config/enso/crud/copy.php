<?php

return [

    /**
     * Class of the Crud Config for Copy.
     */
    'config' => \Yadda\Enso\Copy\Crud\Copy::class,

    /**
     * Class of the Crud Controller for Copy.
     */
    'controller' => \Yadda\Enso\Copy\Controllers\Admin\CopyController::class,

    /**
     * Properties for the Ensō menu item for Copy.
     */
    'menuitem' => [
        'icon' => 'fa fa-clipboard',
        'label' => 'Copy',
        'route' => ['admin.copy.index'],
    ],

    /**
     * Class of the Crud Model for Copy.
     */
    'model' => \Yadda\Enso\Copy\Models\Copy::class,

];
